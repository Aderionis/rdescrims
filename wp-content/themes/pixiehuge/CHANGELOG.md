## [1.1.3] - 2017-11-27

### Changed
- Updated an issue with the teams.
- Added in more countries when adding states to players, teams.
- Updated PixiePanel functions.

## [1.1.0] - 2017-10-21

### Changed
- Added the ability to change the display for number of matches on the all matches page and home page.
- Added the drag & drop functions editing them in the plugin for teams & achievements.
- Removed the character limit for the team subtitle.
- Added a suggested limit for the player quick bio. 
- Resolved the time selection issue for the matches.
- When you don't input a social media link on a player profile, the icon does not appear anymore.
- Resolved an issue on the all matches page.
- When you select a stream for your match now works as intended. 
- The time zone issue has been resolved.
- Updated PixiePanel functions.

## [1.0.5] - 2017-08-13

### Changed
- Updated PixiePanel functions.

## [1.0.4] - 2017-08-12

### Changed
- Added a function, when there are no upcoming matches added, the upcoming match tab does not display
- Redirect added when you delete a player
- Redirect addded when you delete a sponsor
- Deleting maps now work properly
- bbPress visually updated

## [1.0.3] - 2017-08-07

### Changed
- Minor bug fixes & stability changes
- Single stream page added

## [1.0.2] - 2017-07-14

### Changed
- Updated PixiePanel functions.
- Minor bug fixes.

## [1.0.1] - 2017-07-13

### Changed
- Updated PixiePanel functions.
- Improved demo content
- Minor bug fixes.