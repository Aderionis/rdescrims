<?php /* Template Name: All Matches Page */ ?>



<?php get_header(); ?>

	<section id="matches" class="match-page">

        <?php
            get_template_part('inc/match-list');
        ?>

	</section>
	<!-- /MATCHES -->

<?php get_footer(); ?>