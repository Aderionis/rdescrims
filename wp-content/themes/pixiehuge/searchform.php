<form method="get" id="searchform" action="<?php echo site_url() ?>">
	<button type="submit"><i class="fa fa-search"></i></button>
	<input type="text" class="search_field" placeholder="Search" value="" name="s">
	<a class="close" data-dismiss="modal"><i class="fa fa-remove"></i></a>
</form>