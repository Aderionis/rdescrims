<?php /* Template Name: Stream Page */ ?>

<?php
	if(!function_exists('run_pixiehugepanel')) {
	    return false;
	}
	use Alaouy\Youtube\Youtube;

    // Get slug
	$slug = esc_attr(get_query_var('streamname'));

	// Get Team data
	$streamData = pixiehuge_streams(false, false, $slug);

	if(empty($streamData)) {
        wp_redirect('/');
    }
    // Set data
    $stream = $streamData[0];
    $uri = $stream['link']; 
    $chatURL = false;

    // Get information
    if($stream['category'] == 'youtube') {
	    $apiKey = get_option('pixiehuge-youtube-api-key');
	    $youtube = new Youtube($apiKey);
		$user = $youtube->getChannelFromURL($uri);

		if(!$user) {
			return esc_html__('Wrong youtube URL', 'pixiehugepanel');
		}
		$userData = [
			'snippet' => $user->snippet,
			'id'    => $user->id
		];

		$streamURL = '//www.youtube.com/embed/live_stream?channel=' . esc_attr($userData['id']);
		$videoId = false;
		if($getVideoID = file_get_contents('https:' . $streamURL)) {
			if(preg_match('/\'VIDEO_ID\': \"(.*?)\"/', $getVideoID, $matches))
            	$videoId = $matches[1];
		}
		if($videoId) {
			$chatURL = '//www.youtube.com/live_chat?v=' . esc_attr($videoId) . '&embed_domain=' . esc_attr(str_replace(['http://', 'https://'], '', get_site_url()));
		}
		$streamURL .= '&autoplay=1';
    } elseif($stream['category'] == 'twitch') {
    	$streamURL = '//player.twitch.tv/?channel=' . esc_attr($stream['author']);
    	$chatURL = '//www.twitch.tv/' . esc_attr($stream['author']) . '/chat';
    } elseif($stream['category'] == 'mixer') {
    	$streamURL = '//mixer.com/embed/player/' . esc_attr($stream['author']);
    	$chatURL = '//mixer.com/embed/chat/' . esc_attr($stream['author']);
    }
?>

<?php get_header(); ?>

	<div class="container split-flex">
		<section id="stream" class="news-page nobg">
			<div class="section-header">
				<article class="topbar">
					<h3>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="7px" height="8px"><path fill-rule="evenodd"  fill="rgb(57, 191, 253)" d="M-0.000,0.435 C-0.000,0.805 -0.000,7.292 -0.000,7.546 C-0.000,7.877 0.338,8.123 0.672,7.930 C0.940,7.775 6.293,4.649 6.750,4.381 C7.050,4.205 7.045,3.786 6.750,3.611 C6.421,3.415 1.048,0.272 0.658,0.054 C0.373,-0.106 -0.000,0.071 -0.000,0.435 Z"/></svg> Stream page
					</h3>
				</article>
				<!-- /TOP-BAR -->

			</div>
			<!-- /SECTION-HEADER -->

			<div class="content">
				<iframe
					class="stream-embed"
				    src="<?php echo esc_url($streamURL) ?>"
				    height="420"
				    width="100%"
				    frameborder="0"
				    scrolling="no"
				    allowfullscreen="true">
				</iframe>
				<!-- /STREAM-EMBED -->

				<?php if($chatURL): ?>
				<iframe frameborder="0" 
		        	scrolling="no" 
			        id="chat_embed" 
			        src="<?php echo esc_url($chatURL) ?>" 
			        height="500" 
			        width="100%">
				</iframe>
				<!-- /CHAT-EMBED -->
				<?php endif; ?>
			</div>
			<!-- /CONTENT -->

		</section>
		<!-- /STREAM -->

		<?php get_sidebar(); ?>
	</div>
	<!-- /CONTAINER -->

<?php get_footer(); ?>