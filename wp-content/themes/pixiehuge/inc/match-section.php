<?php
$matchsection = get_option('pixiehuge-match-section-enable');

global $sectionNum;
?>

<?php if($matchsection): ?>
	<section id="matches"<?php echo (!empty($sectionNum) && $sectionNum == 1) ? ' class="firstWithBg"' : '' ?>>

        <?php
            get_template_part('inc/match-list');
        ?>

	</section>
	<!-- /MATCHES -->
<?php endif; // Match section ?>