<?php
$wpdb->query("DROP TABLE IF EXISTS `{$tables['teams']}`");
$wpdb->query("CREATE TABLE IF NOT EXISTS `{$tables['teams']}` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `name` varchar(64) NOT NULL,
        `subtitle` varchar(24) NOT NULL,
        `slug` varchar(64) NOT NULL,
        `game_logo` TEXT  NOT NULL,
        `team_logo` TEXT NOT NULL,
        `cover` TEXT NOT NULL,
        `thumbnail` TEXT NOT NULL,
        `stats` TEXT NULL,
        `about` TEXT NULL,
        `country` varchar(48) NULL,
        `my_team` int(11) NOT NULL DEFAULT 0,
        `year_founded` varchar(15) NULL,
        `orderNum` int(11) NULL DEFAULT NULL,
        `created_at` DATETIME NOT NULL
    )" . $wpdb->get_charset_collate() . ";");

$sql = <<<SQL
INSERT INTO `{$tables['teams']}` (`id`, `name`, `subtitle`, `slug`, `game_logo`, `team_logo`, `cover`, `thumbnail`, `stats`, `about`, `country`, `my_team`, `year_founded`, `created_at`) VALUES
(1, 'Midnight Turtles', 'Triple European Winners', 'midnight-turtles', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/gamelogo.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/logo2.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/cover-team-2.jpg', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/Team1.jpg', '{"wins":"125","losses":"42","ties":"91"}', 'SMALL TEAM HUGE PASSION, We are PixieSquad, and we create unique and super awesome themes for gamers. An unlimited passion for design and giant love for a gaming, doing wonders beyond our capabilities.\r\n\r\n&nbsp;\r\n\r\nFrom playing Nintendo, action and shooter games we have always wanted to achieve something in the big wild gaming world.\r\n\r\n&nbsp;\r\n\r\nNow in the modern age, the gaming world is to be called ‘E-Sports’. Where our main focus lies. We have dedicated ourselves to develop our careers in this direction. As it’s where we can show our creativity in the fullest, where passion and crazy effects aren’t a problem, where rules are at the minimum.', 'SE:Sweden', 1, '2016', '2017-06-15 22:38:02'),
(2, 'Rhyno Domynos', 'Two time world champions', 'rhyno-domynos', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/gamelogo1.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/intz.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/Cover-Team.jpg', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/Team2.jpg', '{"wins":"142","losses":"54","ties":"32"}', 'SMALL TEAM HUGE PASSION, We are PixieSquad, and we create unique and super awesome themes for gamers. An unlimited passion for design and giant love for a gaming, doing wonders beyond our capabilities.\r\n\r\n&nbsp;\r\n\r\nFrom playing Nintendo, action and shooter games we have always wanted to achieve something in the big wild gaming world.\r\n\r\n&nbsp;\r\n\r\nNow in the modern age, the gaming world is to be called ‘E-Sports’. Where our main focus lies. We have dedicated ourselves to develop our careers in this direction. As it’s where we can show our creativity in the fullest, where passion and crazy effects aren’t a problem, where rules are at the minimum.', 'NL:Netherlands', 1, '2016', '2017-06-15 22:50:39'),
(3, 'Black Panthers', 'One Time Champions', 'black-panthers', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/gamelogo2.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/logo3.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/cover-sponsors-1.jpg', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/Team3.jpg', '{"wins":"125","losses":"42","ties":"91"}', 'SMALL TEAM HUGE PASSION, We are PixieSquad, and we create unique and super awesome themes for gamers. An unlimited passion for design and giant love for a gaming, doing wonders beyond our capabilities.\r\n\r\n&nbsp;\r\n\r\nFrom playing Nintendo, action and shooter games we have always wanted to achieve something in the big wild gaming world.\r\n\r\n&nbsp;\r\n\r\nNow in the modern age, the gaming world is to be called ‘E-Sports’. Where our main focus lies. We have dedicated ourselves to develop our careers in this direction. As it’s where we can show our creativity in the fullest, where passion and crazy effects aren’t a problem, where rules are at the minimum.', 'SE:Sweden', 1, '2016', '2017-06-15 22:38:02'),
(4, 'Cursed Warriors', 'Four time champs', 'cursed-warriors', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/gamelogo3-1.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/logo4.png', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/cover-team-5.jpg', '//themes.pixiesquad.com/pixiehuge/orange-elite/wp-content/uploads/2017/06/Team4.jpg', '{"wins":"142","losses":"54","ties":"32"}', 'SMALL TEAM HUGE PASSION, We are PixieSquad, and we create unique and super awesome themes for gamers. An unlimited passion for design and giant love for a gaming, doing wonders beyond our capabilities.\r\n\r\n&nbsp;\r\n\r\nFrom playing Nintendo, action and shooter games we have always wanted to achieve something in the big wild gaming world.\r\n\r\n&nbsp;\r\n\r\nNow in the modern age, the gaming world is to be called ‘E-Sports’. Where our main focus lies. We have dedicated ourselves to develop our careers in this direction. As it’s where we can show our creativity in the fullest, where passion and crazy effects aren’t a problem, where rules are at the minimum.', 'NL:Netherlands', 1, '2016', '2017-06-15 22:50:39');
SQL;

$wpdb->query($sql);
